


self.addEventListener('install', function(e) {
 e.waitUntil(
   caches.open('stellation').then(function(cache) {
     return cache.addAll([
       '/',
       '/index.html',
       '/albums.html',
       '/archive.html',
       '/artists.html',
       'contact.html',
       '/brand/logo.svg',
       '/style.css'
     ]);
   })
 );
});


self.addEventListener('fetch', function(event) {

    console.log(event.request.url);
    
    event.respondWith(
    
    caches.match(event.request).then(function(response) {
    
    return response || fetch(event.request);
    
    })
    
    );
    
    });